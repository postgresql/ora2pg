ora2pg (24.3-1) unstable; urgency=medium

  * New upstream version 24.3.

 -- Christoph Berg <myon@debian.org>  Wed, 17 Apr 2024 23:31:47 +0200

ora2pg (24.1-1) unstable; urgency=medium

  * New upstream version 24.1.

 -- Christoph Berg <myon@debian.org>  Sun, 01 Oct 2023 09:42:27 +0200

ora2pg (23.2-1) unstable; urgency=medium

  * New upstream version 23.2.

 -- Christoph Berg <myon@debian.org>  Mon, 07 Nov 2022 15:11:21 +0100

ora2pg (23.1-1) unstable; urgency=medium

  * New upstream version 23.1.
  * debian/tests: Skip tests if mariadb/mysql doesn't start.

 -- Christoph Berg <myon@debian.org>  Thu, 17 Feb 2022 16:56:15 +0100

ora2pg (23.0-1) unstable; urgency=medium

  [ Christoph Berg ]
  * New upstream version 23.0.
  * Prefer mysql-server in tests; mariadb doesn't start on tmpfs.

  [ Debian Janitor ]
  * Update watch file format version to 4.
  * Bump debhelper from old 10 to 13.
  * Set debhelper-compat version in Build-Depends.

 -- Christoph Berg <myon@debian.org>  Fri, 26 Nov 2021 12:47:29 +0100

ora2pg (22.1-2) unstable; urgency=medium

  * Upload to unstable.

 -- Christoph Berg <myon@debian.org>  Mon, 30 Aug 2021 17:22:04 +0200

ora2pg (22.1-1) experimental; urgency=medium

  * New upstream version 22.1.

 -- Christoph Berg <myon@debian.org>  Mon, 05 Jul 2021 11:09:16 +0200

ora2pg (22.0-1) experimental; urgency=medium

  * New upstream version 22.0.

 -- Christoph Berg <myon@debian.org>  Wed, 30 Jun 2021 13:28:14 +0200

ora2pg (21.1-1) experimental; urgency=medium

  * New upstream version 21.1.
  * Fix "Unknown column 'DTD_IDENTIFIER'", upstream #1144.

 -- Christoph Berg <myon@debian.org>  Fri, 23 Apr 2021 13:51:29 +0200

ora2pg (21.0-2) unstable; urgency=medium

  * Prefer mariadb over mysql in autopkgtest.

 -- Christoph Berg <myon@debian.org>  Wed, 18 Nov 2020 10:42:12 +0100

ora2pg (21.0-1) unstable; urgency=low

  [ Debian Janitor ]
  * Trim trailing whitespace.
  * Use secure copyright file specification URI.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.
  * Update standards version to 4.2.1, no changes needed.

  [ Christoph Berg ]
  * New upstream version 21.0.
  * debian/tests: GRANT *.* so the user also gets the global PROCESS privilege
    required in the latest MySQL minor versions.

 -- Christoph Berg <myon@debian.org>  Thu, 15 Oct 2020 21:04:19 +0200

ora2pg (20.0-2) unstable; urgency=medium

  * Add debian/gitlab-ci.yml.

 -- Christoph Berg <myon@debian.org>  Mon, 23 Sep 2019 13:53:10 +0200

ora2pg (20.0-1) unstable; urgency=medium

  * New upstream release.

 -- Christoph Berg <christoph.berg@credativ.de>  Tue, 22 Jan 2019 16:18:16 +0100

ora2pg (19.1-1) unstable; urgency=medium

  * New upstream release.
  * Remove custom compression settings in debian/source/options.

 -- Christoph Berg <christoph.berg@credativ.de>  Mon, 01 Oct 2018 15:37:29 +0200

ora2pg (19.0-1) unstable; urgency=medium

  * New upstream release.
  * Move packaging repository to salsa.debian.org.
  * Move maintainer address to tracker.debian.org.
  * Priority: optional.

 -- Christoph Berg <christoph.berg@credativ.de>  Tue, 28 Aug 2018 12:51:22 +0200

ora2pg (18.2-1) unstable; urgency=medium

  * New upstream release.

 -- Christoph Berg <myon@debian.org>  Sun, 17 Sep 2017 20:07:10 +0200

ora2pg (18.1-1) unstable; urgency=medium

  * Adopt package in the PostgreSQL team. (Closes: #826778)
  * New upstream release.
    + Fixes $$ handling. (Closes: #861443)
  * Update package description.
  * Switch to the primary download location on GitHub.
  * Add autopkgtest.

 -- Christoph Berg <myon@debian.org>  Thu, 15 Jun 2017 23:16:26 +0200

ora2pg (18.0-1) experimental; urgency=medium

  * QA upload
  * New upstream release
  * Patches refreshed
    - 01_Ora2Pg.pod.diff
    - 02_remove_unnecessary_files.diff
  * Patches dropped, applied upstream
    - cf81287191c1560b238fe2967560d8bff33e24b0.patch
    - spelling-fixes.patch
  * Moved out from contrib, as ora2pg now supports both Oracle and MySQL
  * Update copyright years

 -- gustavo panizzo <gfa@zumbi.com.ar>  Wed, 01 Feb 2017 10:33:18 +0800

ora2pg (17.6-1) unstable; urgency=medium

  * QA upload
  * New upstream release
  * Bump Standards-Version to 3.9.8, no changes were needed
  * Add xz on debian/source/options
  * Refresh/Add patches
    - refreshed 01_Ora2Pg.pod.diff
    - refreshed 02_remove_unnecessary_files.diff
      change the manpage's destination from 3 to 3pm,
      not to modify ora2pg script at install time,
      and to install the config in /etc/ora2pg/ora2pg.conf
    - add cf81287191c1560b238fe2967560d8bff33e24b0.patch, reverts 697f09d
      that was breaking encoding with input file (-i)
    - Add spelling-fixes.patch
  * Update compat level to 10
  * Build depend on debhelper >=10
  * Change section from extra to database
  * Update long description to mention MySQL support
  * Update copyright years
  * Add Vcs-* fields to debian/control
  * Update README.source
  * Packaging is now maintained on a git repo under collab-maint
  * Add Vcs fields to debian/control
  * Add pristine-tar to the git repo
  * Add gbp.conf

 -- gustavo panizzo <gfa@zumbi.com.ar>  Sun, 04 Dec 2016 17:13:21 +0800

ora2pg (8.11-2) unstable; urgency=medium

  * QA upload.
  * Set maintainer to Debian QA Group.  (See: #826778)
  * Use dpkg default compressor.  (Closes: #833249)

 -- Andreas Beckmann <anbe@debian.org>  Fri, 02 Dec 2016 09:16:32 +0100

ora2pg (8.11-1) unstable; urgency=low

  * New upstream release.

 -- Julián Moreno Patiño <darkjunix@gmail.com>  Wed, 23 May 2012 21:51:02 -0500

ora2pg (8.10-1) unstable; urgency=low

  * New upstream release.
  * Refresh 02_remove_unnecessary_files.diff
    patch offset.
  * Bump Standards-Version to 3.9.3.
    + Update Homepage in debian/control.
    + Update DEP5 to copyright-format 1.0.
      + Extend copyright holders years.
      + Update URL in Source field.
      + Remove unnecessary newline.
  * Change libcompress-zlib-perl to
    libio-compress-perl in Depends.
  * Remove libstring-random-perl in Depends,
    because is not needed (from upstream changelog).
  * Sort Dependencies and remove newline in debian/control.

 -- Julián Moreno Patiño <darkjunix@gmail.com>  Sun, 25 Mar 2012 00:50:58 -0500

ora2pg (8.9-1) unstable; urgency=low

  * New upstream release.
  * Refresh 02_remove_unnecessary_files.diff patch.

 -- Julián Moreno Patiño <darkjunix@gmail.com>  Wed, 09 Nov 2011 11:04:35 -0500

ora2pg (8.8-1) unstable; urgency=low

  * New upstream release
  * Refresh 02_remove_unnecessary_files.diff patch
  * Move perl to B-D-I
  * debian/rules: Remove override_dh_fixperms
    because upstream doesn't ship anymore
    /usr/share/perl5/ora2pg.pl
  * debian/copyright: Replace DEP5 Format URL from
    dep.debian.net to anonscm.debian.org URL and
    remove unnecessary whitespaces.

 -- Julián Moreno Patiño <darkjunix@gmail.com>  Mon, 17 Oct 2011 13:42:06 -0500

ora2pg (8.7-1) unstable; urgency=low

  * New upstream release
  * Refresh all patches
  * Add override_dh_fixperms to fix permission

 -- Julián Moreno Patiño <darkjunix@gmail.com>  Fri, 09 Sep 2011 01:07:07 -0500

ora2pg (8.6-1) unstable; urgency=low
  * New upstream release
  * Refresh all patches

 -- Julián Moreno Patiño <darkjunix@gmail.com>  Wed, 13 Jul 2011 17:29:21 -0500

ora2pg (8.5-1) unstable; urgency=low

  * New upstream release.
  * Refresh all patches.
  * Set debhelper v8 in B-D
  * Switch compat level 7 to 8

 -- Julián Moreno Patiño <darkjunix@gmail.com>  Wed, 06 Jul 2011 04:19:14 -0500

ora2pg (8.4-1) unstable; urgency=low

  * New upstream release.
  * Update watch file.

 -- Julián Moreno Patiño <darkjunix@gmail.com>  Fri, 10 Jun 2011 23:53:20 -0500

ora2pg (8.3-1) unstable; urgency=low

  * New upstream release
  * Bump Standards-Version to 3.9.2 (no changes).
  * Refresh all patches.

 -- Julián Moreno Patiño <darkjunix@gmail.com>  Mon, 23 May 2011 06:27:38 -0500

ora2pg (8.1-1) unstable; urgency=low

  * New upstream release
  * Remove Suggests in control file
    + Move libdbd-pg-perl to Depends

 -- Julián Moreno Patiño <darkjunix@gmail.com>  Tue, 05 Apr 2011 14:57:08 -0500

ora2pg (8.0-1) unstable; urgency=low

  * New upstream release.
  * Refresh all patches.
  * Update DEP5 format

 -- Julián Moreno Patiño <darkjunix@gmail.com>  Fri, 25 Mar 2011 14:13:52 -0500

ora2pg (7.3-1) unstable; urgency=low

  * New upstream release
  * Removed 02_avoid_ftbfs.diff
    + Merge with upstream
  * Patch to remove unnecessary files

 -- Julián Moreno Patiño <darkjunix@gmail.com>  Wed, 09 Mar 2011 22:03:44 -0500

ora2pg (7.2-1) unstable; urgency=low

  * New upstream release.
  * Upadted 01_Ora2Pg.pod.diff
  * Adopted DEP5 Format
  * Added patch to avoid FTBFS
    + Remove unnecesarry files

 -- Julián Moreno Patiño <darkjunix@gmail.com>  Tue, 25 Jan 2011 17:56:47 -0500

ora2pg (7.1-1) unstable; urgency=low

  * New upstream release.
  * Drop 02_Makefile.PL.diff
    + Now Makefile.PL do it.
  * Updated 01_Ora2Pg.pod.diff
    + Best description.
    + Only fix wrong paths.

 -- Julián Moreno Patiño <darkjunix@gmail.com>  Sun, 05 Dec 2010 12:57:22 -0500

ora2pg (7.0-1) unstable; urgency=low

  * New upstream release.
  * Refreshed all patches.
  * Removed override_dh_installchangelogs
    + Makefile.PL does it
  * Switch compat level 5 to 7

 -- Julián Moreno Patiño <darkjunix@gmail.com>  Thu, 25 Nov 2010 10:23:59 -0500

ora2pg (6.4-1) unstable; urgency=low

  [Julián Moreno Patiño]
  * New upstream release.
  * debian/rules: Use compression package, thanks to Raphaël Hertzog
  * debian/patches: Refreshed all patches.

  [René Mayorga]
  * Add DM-Upload-Allowed

 -- Julián Moreno Patiño <darkjunix@gmail.com>  Sun, 19 Sep 2010 10:14:09 -0500

ora2pg (6.3-1) unstable; urgency=low

  * New upstream release.
  * Bump Standards-Version to 3.9.1 (no changes).
  * debian/patches: Refreshed all patches.

 -- Julián Moreno Patiño <darkjunix@gmail.com>  Sat, 07 Aug 2010 17:30:46 -0500

ora2pg (6.2-1) unstable; urgency=low

  * New upstream release.
  * debian/patches: Refreshed all patches.

 -- Julián Moreno Patiño <darkjunix@gmail.com>  Sat, 19 Jun 2010 18:52:06 -0500

ora2pg (6.1-1) unstable; urgency=low

  * New upstream release.
  * debian/watch: Adjusted regular expression.
  * debian/rules: Used dh_auto_configure instead perl Makefile.PL
  * debian/patches: Updated.
  * debian/copyright: Copyright dates updated.

 -- Julián Moreno Patiño <darkjunix@gmail.com>  Sun, 23 May 2010 14:07:19 -0500

ora2pg (6.0-1) unstable; urgency=low

  * New upstream release.
  * debian/control: Changed cdbs to debhelper 7.0.50~ from B-D
  * debian/control: Add quilt to B-D
  * debian/rules: Used debhelper 7.0.50~ overrides
  * debian/copyright: Updated the license.
  * debian/patches: Fixed lintian errors.
  * debian/watch: Updated regular expression.
  * Updated standards version.

 -- Julián Moreno Patiño <darkjunix@gmail.com>  Sun, 21 Mar 2010 12:05:11 -0500

ora2pg (5.5-1) unstable; urgency=low

  * New upstream release.
  * debian/source: Added support debsrc3
  * debian/README.source: Added.
  * debian/control: Removed ${shlibs:Depends} from Depends
  * debian/copyright: Updated.
  * Repacked upstream tarball to bzip2 -9 compression

 -- Julián Moreno Patiño <darkjunix@gmail.com>  Mon, 28 Dec 2009 17:00:11 -0500

ora2pg (5.4-1) unstable; urgency=low

  * New upstream release.
  * debian/copyright: Update copyright date.
  * debian/control: Changed Maintainer field.
  * debian/control: Fix debhelper-but-no-misc-depends lintian warning.
  * debian/rules: Added library PLSQL.pm and generate man.
  * Updated standards version.
  * New maintainer. Closes: #548323
  * move perl from B-D, to B-D-I

 -- Julián Moreno Patiño <darkjunix@gmail.com>  Sat, 31 Oct 2009 16:17:56 -0500

ora2pg (4.11-1) unstable; urgency=low

  * New upstream release

 -- Peter Eisentraut <petere@debian.org>  Wed, 07 Jan 2009 00:33:51 +0200

ora2pg (4.9-1) unstable; urgency=low

  * New upstream release
  * Updated standards version

 -- Peter Eisentraut <petere@debian.org>  Sat, 15 Nov 2008 12:42:19 +0200

ora2pg (4.7-1) unstable; urgency=low

  * New upstream release
    - Obsoletes string-compare.patch

 -- Peter Eisentraut <petere@debian.org>  Tue, 29 Jan 2008 11:37:56 +0100

ora2pg (4.6-1) unstable; urgency=low

  * New upstream release
  * Updated standards version
  * Changed Homepage location and watch file to pgFoundry

 -- Peter Eisentraut <petere@debian.org>  Mon, 07 Jan 2008 14:38:16 +0100

ora2pg (4.5-2) unstable; urgency=medium

  * libcompress-zlib-perl is a Depends

 -- Peter Eisentraut <petere@debian.org>  Tue, 20 Nov 2007 12:30:34 +0100

ora2pg (4.5-1) unstable; urgency=low

  * Initial release

 -- Peter Eisentraut <petere@debian.org>  Wed, 24 Oct 2007 15:33:06 +0200
